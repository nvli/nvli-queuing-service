package in.gov.nvli.nqs.utils;

/**
 *
 * @author Gulafsha
 * @author Vivek Bugale <bvivek@cdac.in>
 */
public class Constants {
    
    /*SMTP Properties */    
     public static final String SMTP = "smtp";
     public static final String SMTP_HOST = "mail.smtp.host";
     public static final String SMTP_PORT= "mail.smtp.port";
     public static final String SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
     public static final String SMTP_USER = "mail.smtp.user";
     public static final String SMTP_PASSWORD= "mail.smtp.password";
     public static final String SMTP_AUTH = "mail.smtp.auth";
     
    /*System Property*/
    public static final String LINE_SEPARATOR = "line.separator";
    
    /*Email exchange name*/
    public static final String EMAIL_EXCHANGE = "nvli.email.exchange";
    
    /*Queue keys*/
    public static final String EMAIL_RECOVERY_KEY = "emailRecoveryKey";
    public static final String EMAIL_VERIFICATION_KEY = "emailVerificationKey";
    public static final String EMAIL_NVLI_SHARE_KEY = "emailNvliShareKey";
    public static final String EMAIL_INVITATION_KEY = "emailInvitationKey";
    
    /*HTML template file path*/
    public static final String DEFAULT_EMAIL_TEMPLATE_FILE_PATH = "email-templates/default-template.html";
    public static final String NVLI_SHARE_EMAIL_TEMPLATE_FILE_PATH = "email-templates/nvliShare-template.html";
    
    /*HTML content to be replaced*/
    public static final String HTML_SUBMIT = "{{SUBMIT}}";
    public static final String HTML_SENDER_NAME = "{{SENDERNAME}}";
    public static final String HTML_RECORD_TITLE = "{{RECORDTITLE}}";
    public static final String HTML_IMAGE_URL = "{{IMAGEURL}}";
    public static final String HTML_USER = "{{USER}}";
    public static final String HTML_MESSAGE = "{{MESSAGE}}";
    public static final String HTML_EMAIL_MESSAGE_LINK = "{{EMAILMSGLINK}}";
     
    /*Submit button options*/       
    public static final String EMAIL_VERIFICATION = "Email Verification";
    public static final String RESET_PASSWORD = "Reset Password";
    public static final String NVLI_INVITATION = "NVLI Invitation";
    public static final String EMAIL_VERIFICATION_SUBMIT = "Activate your account";
    public static final String RESET_PASSWORD_SUBMIT = "Reset password";
    public static final String NVLI_INVITATION_SUBMIT= "Register here";
    public static final String DEFAULT_SUBMIT = "Click here";
    
    /*MIME type of object*/
    public static final String TEXT_HTML = "text/html";
    
    /*Email Details*/
    public static final String FIRST_NAME = "firstName";
    public static final String USER_MAIL_ID = "userMailId";
    public static final String SUBJECT = "subject";
    public static final String MESSAGE = "message";
    public static final String SENDER_FIRST_NAME = "senderFirstName";
    public static final String RECORD_TITLE = "recordTitle";
    public static final String IMAGE_URL = "imageUrl";
    public static final String SERVLET_CONTEXT_NAME = "servletCtxName";
    public static final String CONTROLLER_MAPPING = "controllerMapping";
    public static final String SALT = "salt";
    
    /*URL Properties*/
    public static final String SEPARATOR = "/";
    public static final String IDH ="?idh=";
    public static final String ST = "&st=";
    public static final String UTF_8 = "UTF-8";
}
