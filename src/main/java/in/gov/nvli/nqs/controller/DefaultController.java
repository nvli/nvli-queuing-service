package in.gov.nvli.nqs.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author akinza
 * @author Vivek Bugale <bvivek@cdac.in>
 */
@Controller
public class DefaultController {    
    
     private static final Logger LOG = Logger.getLogger(DefaultController.class);
    
    @RequestMapping(value = "/")
    public String index() {
        LOG.info("Welcome to NVLI Queuing Service.");        
        return "index";
    }
}
