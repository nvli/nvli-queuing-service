package in.gov.nvli.nqs.service;

import in.gov.nvli.nqs.utils.Constants;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.GregorianCalendar;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.xml.bind.DatatypeConverter;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

/**
 * <p>
 * Sending mail and its utilities.
 * <p>
 *
 * @author Vivek Bugale <bvivek@cdac.in>
 */
@Component
public class SendMailService {

    /**
     *
     * <p>
     * Responsible for capturing logging information of SendMailService.</p>
     */
    private static final Logger LOG = Logger.getLogger(SendMailService.class);

    /**
     * Project URL
     */
    @Value("${project.url}")
    String projectURL;    
    
    /**
     * NVLI email id.
     */
    @Value("${nvli.email.id}")
    String nvliEmailId;
    
    /**
     * SMTP User
     */
    @Value("${mail.smtp.user}")
    String mailSmtpUser;

    /**
     * SMTP password.
     */
    @Value("${mail.smtp.password}")
    String mailSmtpPassword;

    /**
     * SMTP host.
     */
    @Value("${mail.smtp.host}")
    String smtpHost;

    /**
     * SMTP port
     */
    @Value("${mail.smtp.port}")
    int smtpPort;
    
    /**
     * STARTTLS enable
     */
    @Value("${mail.smtp.starttls.enable}")
    String isStarttlsEnable;
    
    /**
     * SMTP AUTH
     */
    @Value("${mail.smtp.auth}")
    String smtpAuth;

    /**
     * <p>
     * Set system properties. </p>
     *
     * @param systemProperties
     */
    private void setSystemProperties(Properties systemProperties) {

        if (mailSmtpUser.equals(nvliEmailId)) {
//            This code for info@nvli.in
            systemProperties.put(Constants.SMTP_HOST, smtpHost);
            systemProperties.put(Constants.SMTP_PORT, smtpPort);
        } else {
//            This code for nvli - noreply@cdac.in
            systemProperties.put(Constants.SMTP_STARTTLS_ENABLE,isStarttlsEnable);
            systemProperties.put(Constants.SMTP_USER, mailSmtpUser);
            systemProperties.put(Constants.SMTP_PASSWORD, mailSmtpPassword);
            systemProperties.put(Constants.SMTP_PORT, smtpPort);
            systemProperties.put(Constants.SMTP_AUTH, smtpAuth);
        }
    }

    /**
     * <p>
     * This method is used to send mail. </p>
     *
     * @param key This is routing key
     * @param emailDetailsJson
     *
     */
    public void sendMail(String key, JSONObject emailDetailsJson) {
        //check emailDetailsJson is empty.
        if (!emailDetailsJson.isEmpty()) {
            try {
                LOG.info("Simple mail sending using template start ...");
                //get system properties
                Properties properties = System.getProperties();
                //set System Properties
                setSystemProperties(properties);
                //get session
                Session session = Session.getDefaultInstance(properties);
                //email Transport
                //check key
                emailTransport(session, setEmailProperties(session, key, emailDetailsJson));
            } catch (MessagingException | IOException ex) {
                LOG.error(ex.getMessage(), ex);
            }
            LOG.info("Simple mail sending using template end !!!");
        }
    }

    /**
     * <p>
     * This is email transport. </p>
     *
     * @param session
     * @param message
     * @throws MessagingException
     */
    private void emailTransport(Session session, MimeMessage message) throws MessagingException {
        if (mailSmtpUser.equals(nvliEmailId)) {
//            This code for info@nvli.in
            Transport.send(message);
        } else {
//            This code for nvli-noreply@cdac.in
            Transport transport = session.getTransport(Constants.SMTP);
            transport.connect(smtpHost, mailSmtpUser, mailSmtpPassword);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
    }

    /**
     * <p>
     * This method is used to set MimeMessage properties. </p>
     *
     * @param session
     * @param key This is routing key
     * @return MimeMessage
     * @throws MessagingException
     * @throws IOException
     */
    private MimeMessage setEmailProperties(Session session, String key, JSONObject emailDetailsJson) throws MessagingException, IOException {
        MimeMessage message = new MimeMessage(session);
        MimeMultipart multipart = new MimeMultipart();
        BodyPart messageBodyPart = new MimeBodyPart();

        messageBodyPart.setContent(readEmailFromHtml(key, emailDetailsJson), Constants.TEXT_HTML);
        multipart.addBodyPart(messageBodyPart);

        message.setFrom(new InternetAddress(mailSmtpUser));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress((String) emailDetailsJson.get(Constants.USER_MAIL_ID)));
        message.setSubject((String) emailDetailsJson.get(Constants.SUBJECT));
        message.setContent(multipart);
        return message;
    }

    /**
     * <p>
     * This method return email template as String. </p>
     *
     * @param key This is routing key
     * @return String
     * @throws IOException
     */
    private String readEmailFromHtml(String key, JSONObject emailDetailsJson) throws IOException {
        String fileContent = readContentFromFile(getFilePath(key));
        fileContent = fileContent.replace(Constants.HTML_USER, (String) emailDetailsJson.get(Constants.FIRST_NAME));
        fileContent = fileContent.replace(Constants.HTML_MESSAGE, (String) emailDetailsJson.get(Constants.MESSAGE));
        fileContent = fileContent.replace(Constants.HTML_EMAIL_MESSAGE_LINK, fetchEmailMsgLink(emailDetailsJson));
        switch (key) {
            case Constants.EMAIL_INVITATION_KEY:
            case Constants.EMAIL_RECOVERY_KEY:
            case Constants.EMAIL_VERIFICATION_KEY:
                return setDefaultTemplateOption(fileContent, (String) emailDetailsJson.get(Constants.SUBJECT));
            case Constants.EMAIL_NVLI_SHARE_KEY:
                return setNVLIShareTemplateOptions(fileContent, emailDetailsJson);
        }
        return fileContent;
    }

    /**
     *
     * <p>
     * This method is used to read HTML file as a String. </p>
     *
     * @param fileName
     * @return String
     * @throws IOException
     */
    private String readContentFromFile(String fileName) throws IOException {
        StringBuilder contents = new StringBuilder();
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            String line;
            while ((line = reader.readLine()) != null) {
                contents.append(line);
                contents.append(System.getProperty(Constants.LINE_SEPARATOR));
            }
        }catch(Exception ex){
            LOG.error("Error while reading content from file :: "+ex.getMessage(),ex);
        }
        return contents.toString();
    }

    /**
     *
     * <p>
     * Get file path using routing key. </p>
     *
     * @param key This is routing key
     * @return String
     * @throws IOException
     */
    private String getFilePath(String key) throws IOException {
        switch (key) {
            case Constants.EMAIL_INVITATION_KEY:
            case Constants.EMAIL_RECOVERY_KEY:
            case Constants.EMAIL_VERIFICATION_KEY:
                return new ClassPathResource(Constants.DEFAULT_EMAIL_TEMPLATE_FILE_PATH).getFile().getPath();
            case Constants.EMAIL_NVLI_SHARE_KEY:
                return new ClassPathResource(Constants.NVLI_SHARE_EMAIL_TEMPLATE_FILE_PATH).getFile().getPath();
        }
        return null;
    }

    /**
     *
     * <p>
     * This method return email message link. </p>
     *
     * @return String
     */
    private String fetchEmailMsgLink(JSONObject emailDetailsJson) {
        return !emailDetailsJson.isEmpty() ? projectURL + (String) emailDetailsJson.get(Constants.SERVLET_CONTEXT_NAME) + Constants.SEPARATOR + (String) emailDetailsJson.get(Constants.CONTROLLER_MAPPING) + Constants.IDH + (String) emailDetailsJson.get(Constants.SALT) + Constants.ST + getEncodedSendDate() : null;
    }

    /**
     *
     * <p>
     * Returns encoded date. </p>
     *
     * @return String
     */
    private String getEncodedSendDate() {
        try {
            return DatatypeConverter.printBase64Binary(new GregorianCalendar().getTime().toString().getBytes(Constants.UTF_8));
        } catch (UnsupportedEncodingException ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return null;
    }

    /**
     *
     * <p>
     * Set default template option. </p>
     *
     * @param fileContent
     * @param subject
     * @return String
     */
    private String setDefaultTemplateOption(String fileContent, String subject) {
        //default template
        switch (subject) {
            case Constants.EMAIL_VERIFICATION:
                return fileContent.replace(Constants.HTML_SUBMIT, Constants.EMAIL_VERIFICATION_SUBMIT);
            case Constants.RESET_PASSWORD:
                return fileContent.replace(Constants.HTML_SUBMIT, Constants.RESET_PASSWORD_SUBMIT);
            case Constants.NVLI_INVITATION:
                return fileContent.replace(Constants.HTML_SUBMIT, Constants.NVLI_INVITATION_SUBMIT);
            default:
                return fileContent.replace(Constants.HTML_SUBMIT, Constants.DEFAULT_SUBMIT);
        }
    }

    /**
     *
     * <p>
     * Set NVLI share template options. </p>
     *
     * @param fileContent
     * @return String
     */
    private String setNVLIShareTemplateOptions(String fileContent, JSONObject emailDetailsJson) {
        //NVLI share template
        return fileContent
                .replace(Constants.HTML_SENDER_NAME, (String) emailDetailsJson.get(Constants.SENDER_FIRST_NAME))
                .replace(Constants.HTML_RECORD_TITLE, (String) emailDetailsJson.get(Constants.RECORD_TITLE))
                .replace(Constants.HTML_IMAGE_URL, (String) emailDetailsJson.get(Constants.IMAGE_URL));
    }
}
