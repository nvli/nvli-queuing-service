package in.gov.nvli.nqs.consumer;

import in.gov.nvli.nqs.service.SendMailService;
import in.gov.nvli.nqs.utils.Constants;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * <p>
 * The NvliShareConsumer consumes "queue.email.nvliShare" queue. </p>
 *
 *
 * @author Vivek Bugale <bvivek@cdac.in>
 */
public class NvliShareConsumer implements MessageListener {

    /**
     *
     * <p>
     * Responsible for capturing logging information of NvliShareConsumer.</p>
     */
    private static final Logger LOG = Logger.getLogger(NvliShareConsumer.class);

    @Autowired
    private SendMailService mailService;

    /**
     *
     * <p>
     * Passes a message to the listener.</p>
     *
     * @param msg
     */
    @Override
    public void onMessage(Message msg) {
        try {
            LOG.info("[**] email received @NvliShareConsumer from the queue");
            JSONObject emailDetailsJson = (JSONObject)  new JSONParser().parse(new String(msg.getBody()));
            //check require parameters for email sending
            if (!emailDetailsJson.isEmpty()) {
                //send mail
                mailService.sendMail(Constants.EMAIL_NVLI_SHARE_KEY, emailDetailsJson);
            }
            LOG.info(" [**] NVLI share email sent.");
        } catch (ParseException ex) {
            LOG.error(" [**] NVLI share email sending error ::"+ex.getMessage(), ex);
        }
    }

}
