/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.nqs.consumer;

import in.gov.nvli.nqs.service.SendMailService;
import in.gov.nvli.nqs.utils.Constants;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * <p>
 * The EmailVerificationConsumer consumes "queue.email.verification" queue. </p>
 *
 * @author Vivek Bugale <bvivek@cdac.in>
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public class EmailVerificationConsumer implements MessageListener {

    /**
     *
     * <p>
     * Responsible for capturing logging information of
     * EmailVerificationConsumer.</p>
     */
    private static final Logger LOG = Logger.getLogger(EmailVerificationConsumer.class);

    @Autowired
    private SendMailService mailService;

    /**
     * <p>
     * Passes a message to the listener.</p>
     *
     * @param msg
     */
    @Override
    public void onMessage(Message msg) {
        try {
            LOG.info("[****] email received @verificationConsumer from the queue");
            JSONObject emailDetailsJson = (JSONObject) new JSONParser().parse(new String(msg.getBody()));
            //check require parameters for email sending
            if (!emailDetailsJson.isEmpty()) {
                //send mail
                mailService.sendMail(Constants.EMAIL_VERIFICATION_KEY, emailDetailsJson);
            }
           LOG.info(" [****] verification email sent.");
        } catch (ParseException ex) {
            LOG.error("[****] verification email sending error :: "+ex.getMessage(), ex);
        }
    }
}
